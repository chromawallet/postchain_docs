===================
System requirements
===================

Postchain Node JS
^^^^^^^^^^^^^^^^^

Operating system: Linux

Software dependencies:

* Software packages required, for Ubuntu Linux: python libpq-dev make g++
* PostgeSQL 9.4 or later
* NodeJS 7 or later

Postchain Java
^^^^^^^^^^^^^^

Operating system: Any operating system that can run the software dependencies. Linux is recommended.

Software dependencies:

* PostgeSQL 9.4 or later.
* Java 8 Runtime Environment. Either OpenJDK's JRE or Oracle's JRE.

Hardware requirements
^^^^^^^^^^^^^^^^^^^^^

Hardware requirements generally depend on the nature of the application.

For small-scale demos we recommend 2 GB RAM, two CPU core
(or virtual CPUs in a virtualized environment) and at least 10 GB of disk space.

These are not the minimal requirements, however.
