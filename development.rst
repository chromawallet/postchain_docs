=============================
Application developer's guide
=============================

Implementing blockchain logic

In most typical case, to implement custom blockchain you only need to implement GTX operations and module, which can then be plugged into the rest of the system.

This can be done in three steps:

1. Define schema in an SQL file. It should defined tables and stored procedures needed by the application.
2. Define GTX operations by subclassing net.postchain.gtx.GTXOperation.
3. Define GTX module which maps names to operations, by subclassing net.postchain.gtx.GTXModule interface, or subclassing net.postchain.gtx.SimpleGTXModule

* Optionally, if GTX module needs parameters, one can define GTX module factory (net.postchain.gtx.GTXModuleFactory).

GTXTestOp is an example of a simple GTX operation which simply runs INSERT query with user-provided string. It is included into GTXTestModule which includes schema in an inline string and also includes an example of a query implementation.

More complex example can be found in the turorial.

If it is necessary to use custom transaction format, it can be done in following way:

1. Implement net.postchain.core.Transaction interface to define transaction serialization format and semantics.
2. Implement transaction factory (net.postchain.core.TransactionFactory), usually it just calls transaction constructor.
3. Implement BlockchhainConfiguration. Simplest way is to subclass net.postchain.BaseBlockchainConfiguration and override getTransactionFactory method.
4. Implement BlockchainConfigurationFactory.

Steps 2-4 are usually trivial.
