====================
Postchain components
====================

To enable flexibility in different modes of operation and variants of use, Postchain is structured into several components, or layers:

* Framework defines base classes and interfaces. It is general enough to be used for all kinds of scenarios: custom blockchains (public, private, federated), sidechains, blockchain indexers. Framework doesn't "do" much, but it helps to organize the code, enables modularity and interoperability.
* Kit is a toolkit for custom private/federate blockchains.

In other words, Framework is an abstract thing, while  Kit is much more specialized. Kit defines concrete message formats, algorithms, implementations and so on.

Framework and Kit is all we do for initial "MVP" release, in future we will also implement configuraton and deployment tools, either as a separate component or as a part of Kit.

Framework
^^^^^^^^^

The goal of Postchain Framework is to define interfaces which help with code organization, enable interoperabiity between different modules. It also implements several commonly used classes and functions.

Framework should be flexible enough for following scenarios:

* building custom blockchains (public or private/federated)
* indexing data of an existing blockchain (Bitcoin, Ethereum, ...)
* implementing sidechains (which combine custom blockchains with public blockchains)

What is included in Framework:

* class interfaces (implied): Block, Transaction, BlockFactory, TransactionFactory
* base implementations: AbstractBlock, BlockFactory, TransactionFactory
* utilities: Storage (pg connector), crypto utilities
* "example" implementations: BlockStore, ByteTx, ByteTxStore, SimpleSignedBlock

It's not clear if "example implementations" are actually a part of the Framework, or are just examples.

Perhaps Framework should include more stuff, like logging.

Since JS has no notion of interfaces, they will be defined in documentation.

Framework documentation should also include recommendations on schema design, best practices, etc.

Kit
^^^

The goal of Kit is to provide tools for building custom blockchains, particularly enterprise private/federated blockchains. Ideally, it should be possible to use it both as a complete solution, and as a collection of building blocks and helpers.

Kit includes the following component:

* inter-peer networking, with ASN.1-based message format and ECDSA message signing
* an implementation of Block which is designed for proof-of-authority kind of blockchain (having a room for validator signatures)
* a PBFT-derived consensus algorithm, designed to work with aforementioned Block implementation
* compiler for a Ratatosk-like language which is used to define transaction implementations (it is also possible to implement it in a custom way)
* client-facing RPC server (submit transactions, query-data)
* client SDK
* some minimal configuration

Network
^^^^^^^

Network components take care of "boring" stuff like messaging, signing messages, checking message signatures.

MVP implementaiton will use a rudimentary static configuration, future versions might use dynamic reconfiguration.

Consensus algorithm
^^^^^^^^^^^^^^^^^^^

Consensus algorithm is inspired by Castro's PBFT, but works on block level and restricts concurrency as much as possible. (It seems IBM is taking similar approach in hyperledger/fabric/consensus/simplebft.)

Block implementation
^^^^^^^^^^^^^^^^^^^^

Might be very similar to SimpleSignedBlock, except with ASN.1 serialization format.

Compiler
^^^^^^^^

A Ratatosk-like language can be used to define a list of actions::

    (defblockchain ledger
        (actions
            (send-money ((from :type pubkey) (to :type pubkey) (amount :type integer))
	        (guard (signatures from))
	        (sql (foobar_send_money from to amount)))))

A compiler will take this description and use it to define:

* ASN.1 serialization format for transaction
* Transaction classes which handle deserialization and check authorization according to specified guard clauses
* Store classes

Together with SQL schema definition this is enough to define a custom blockchain in the context of Kit.

Client RPC
^^^^^^^^^^

Postchain nodes will both serve as peers in a Postchain network, and serve as a RPC server for clients.

Clients should be able to:

* submit a transaction; transaction is automatically routed to a primary
* check transaction status
* query  blocks & transactions	
* (TBD) transaction creation helper (client sends JSON, server constructs & signs transaction)
* (TBD) query blockchain data (???)

One problem is that queries are blockchain-specific. We can either leave that to blockchain
implementors (it's not hard to query the DB and return data...), but we might also make use of
aforementioned compiler & language to define blockchain-specific queries.

(Another possibility is to user a 3rd-party REST API for read queries, e.g. Postgrest.)

Client SDK
^^^^^^^^^^

Client SDK should include tools to construct transactions & sign them. It might be necessary to link with compiler-generated message codecs. (One option is for compiler to generate SDK bundle for a specific blockchain.)

Configuration
^^^^^^^^^^^^^

We should make it easy for user to launch a Postchain Kit server for a specific blockchain(s)
and using a pre-defined network peer configuration. In the initial version we only need to
make it work with a static configuration.
