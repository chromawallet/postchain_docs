======
README
======

Text is written in reStructuredText (http://docutils.sourceforge.net/rst.html) and the idea is to use Read the Docs (https://readthedocs.org/).

I have basically just created the basic structure and copied stuff from the Postchain repository. I'm not sure, for example, if the Framework/Kit description is still appropriate.
